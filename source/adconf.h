// -------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// -------------------------------------------------------------------------


#ifndef __ADCONF_H
#define __ADCONF_H

#include <stdio.h>
#include "global.h"


#define DEG(x) ((x) * 57.29578f)
#define RAD(x) ((x) / 57.29578f)


enum
{
    ACN_W,
    ACN_Y, ACN_Z, ACN_X,
    ACN_V, ACN_T, ACN_R, ACN_S, ACN_U,
    ACN_Q, ACN_O, ACN_M, ACN_K, ACN_L, ACN_N, ACN_P
};


enum 
{
    CM_M0 = 0x0001,
    CM_M1 = 0x000E,
    CM_M2 = 0x01F0,
    CM_M3 = 0xFE00,
    CM_3D = 0x7CE4
};


class Speaker
{
public:

    Speaker (void);
    void reset (void);
    void rae2xyz (void);

    float   _r;
    float   _a;
    float   _e;
    float   _x;
    float   _y;
    float   _z;
    char    _label [4];
    char    _jackp [64];
};


class Matrow
{
public:

    Matrow (void);
    void reset (void);
    void encode (int cmask, float az, float el);
    void encode (int cmask, float x, float y, float z);
    void check (void);

    float   _coeff [16];
};


class AD_conf
{
public:

    enum { N3D, SN3D, FUMA };
    enum { OFF, ON };
    enum { NONE, INPUT, OUTPUT };
    enum { ST_INPS = 1, ST_OUTS = 2, ST_OPTS = 4, ST_MATR = 8, ST_ALL = 15 };

    AD_conf (void);
    void reset (void);
    int  load (const char *file);
    int  save (const char *file);

    unsigned int   _fform;
    unsigned int   _state;
    char           _fname [1024];
    char           _descr [128];
    struct
    {
	int   cmask;
	int   nband;
	int   nspkr;
	int   scale;
    }              _dec;
    struct
    {
        int   scale;
	int   nfeff;
        int   delay;
        int   level;
	float xfreq;
	float ratio;
    }              _opt;
    Speaker        _speakers [MAXOP];
    Matrow         _lfmatrix [MAXOP];
    Matrow         _hfmatrix [MAXOP];
    float          _lfgain [4];
    float          _hfgain [4];

    // For compatibility with version 2 files only.
    int        _h_ord;
    int        _v_ord;
    const int *_chmap;

    void default_label (void);
    void default_jackp (void);

private:

    enum { NOERR, ERROR, COMM, ARGS, EXTRA, SCOPE };

    void save_matrix (FILE *, const char *, Matrow *, float *);
    void makechmap (void);
    int  read_matrow2 (char **, Matrow *);
    int  read_matrow3 (char **, Matrow *);
};    


#endif
