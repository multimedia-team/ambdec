// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#ifndef __NFFILT_H
#define __NFFILT_H


class NF_filt1
{
public:

    NF_filt1 (void) {}
    ~NF_filt1 (void) {}

    void init (float w);
    void init (NF_filt1& F);
    void reset (void) { _z1 = 0; }
    void process (int n, float *ip, float *op, float gain);
    void process_add (int n, float *ip, float *op, float gain);

private:

    float _g;
    float _d1;
    float _z1;
};


class NF_filt2
{
public:

    NF_filt2 (void) {}
    ~NF_filt2 (void) {}

    void init (float w);
    void init (NF_filt2& F);
    void reset (void) { _z1 = _z2 = 0; }
    void process (int n, float *ip, float *op, float gain);
    void process_add (int n, float *ip, float *op, float gain);

private:

    float _g;
    float _d1, _d2;
    float _z1, _z2;
};


class NF_filt3
{
public:

    NF_filt3 (void) {}
    ~NF_filt3 (void) {}

    void init (float w);
    void init (NF_filt3& F);
    void reset (void) { _z1 = _z2 = _z3 = 0; }
    void process (int n, float *ip, float *op, float gain);
    void process_add (int n, float *ip, float *op, float gain);

private:

    float _g;
    float _d1, _d2, _d3;
    float _z1, _z2, _z3;
};


class NF_filt4
{
public:

    NF_filt4 (void) {}
    ~NF_filt4 (void) {}

    void init (float w);
    void init (NF_filt4& F);
    void reset (void) { _z1 = _z2 = _z3 = _z4 = 0; }
    void process (int n, float *ip, float *op, float gain);
    void process_add (int n, float *ip, float *op, float gain);

private:

    float _g;
    float _d1, _d2, _d3, _d4;
    float _z1, _z2, _z3, _z4;
};


#endif
