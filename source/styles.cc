// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#include "styles.h"


XftColor        *XftColors [NXFTCOLORS];
XftFont         *XftFonts [NXFTFONTS];
X_button_style   Bst0, Bst1, Bst2, Bst3;
X_textln_style   Tst0, Tst1, Tst2;
X_slider_style   Fst1;
X_mclist_style   Lst0; 
X_scale_style    Sdb1, Sdb2;


void styles_init (X_display *disp, X_resman *xrm)
{
    XftColors [C_MAIN_BG]  = disp->alloc_xftcolor (xrm->get (".color.main.bg",   "gray30"),  0);
    XftColors [C_MAIN_LS]  = disp->alloc_xftcolor (xrm->get (".color.main.ls",   "gray60"),  0);
    XftColors [C_MAIN_DS]  = disp->alloc_xftcolor (xrm->get (".color.main.ds",   "black"),   0);
    XftColors [C_MAIN_SB]  = disp->alloc_xftcolor (xrm->get (".color.main.sb",   "gray40"),  0);
    XftColors [C_DISP_BG]  = disp->alloc_xftcolor (xrm->get (".color.disp.bg",   "gray10"),  0);
    XftColors [C_TEXT_BG]  = disp->alloc_xftcolor (xrm->get (".color.text.bg",   "white"),   0);
    XftColors [C_TEXT_HL]  = disp->alloc_xftcolor (xrm->get (".color.text.hl",   "#c0ffa0"), 0);
    XftColors [C_TEXT_CA]  = disp->alloc_xftcolor (xrm->get (".color.text.ca",   "red"),     0);
    XftColors [C_TEXT_GR]  = disp->alloc_xftcolor (xrm->get (".color.text.gr",   "gray80"),  0);
    XftColors [C_TEXT_MO]  = disp->alloc_xftcolor (xrm->get (".color.text.er",   "#e0e0c0"), 0);
    XftColors [C_TEXT_ER]  = disp->alloc_xftcolor (xrm->get (".color.text.er",   "coral"),   0);
    XftColors [C_BUTT_BG0] = disp->alloc_xftcolor (xrm->get (".color.butt.bg0",  "gray40"),  0);
    XftColors [C_BUTT_BG1] = disp->alloc_xftcolor (xrm->get (".color.butt.bg1",  "green"),   0);
    XftColors [C_BUTT_BG2] = disp->alloc_xftcolor (xrm->get (".color.butt.bg2",  "orange"),  0);
    XftColors [C_BUTT_BG3] = disp->alloc_xftcolor (xrm->get (".color.butt.bg3",  "gray80"),  0);
    XftColors [C_BUTT_FG0] = disp->alloc_xftcolor (xrm->get (".color.butt.fg0",  "white"  ), 0);
    XftColors [C_BUTT_FG1] = disp->alloc_xftcolor (xrm->get (".color.butt.fg1",  "black"  ), 0);
    XftColors [C_BUTT_FG2] = disp->alloc_xftcolor (xrm->get (".color.butt.fg2",  "black"  ), 0);
    XftColors [C_BUTT_FG3] = disp->alloc_xftcolor (xrm->get (".color.butt.fg3",  "black"  ), 0);
    XftColors [C_SLID_KN]  = disp->alloc_xftcolor (xrm->get (".color.slid.kn",   "yellow"),  0);
    XftColors [C_SLID_MK]  = disp->alloc_xftcolor (xrm->get (".color.slid.mk",   "black"),   0);
    XftColors [C_MAIN_FG]  = disp->alloc_xftcolor (xrm->get (".color.main.fg",   "white"  ), 0);
    XftColors [C_MAIN_HL]  = disp->alloc_xftcolor (xrm->get (".color.main.hl",   "#a0a0ff"), 0);
    XftColors [C_TEXT_FG]  = disp->alloc_xftcolor (xrm->get (".color.text.fg",   "black"  ), 0);
    XftColors [C_TICK0]    = disp->alloc_xftcolor (xrm->get (".color.tick0",     "black"  ), 0);
    XftColors [C_TICK1]    = disp->alloc_xftcolor (xrm->get (".color.tick1",     "yellow" ), 0);

    XftFonts [F_FILE] = disp->alloc_xftfont (xrm->get (".font.file", "luxi:bold:pixelsize=12"));
    XftFonts [F_TEXT] = disp->alloc_xftfont (xrm->get (".font.text", "luxi:pixelsize=11"));
    XftFonts [F_BUTT] = disp->alloc_xftfont (xrm->get (".font.butt", "luxi:pixelsize=11"));
    XftFonts [F_SCAL] = disp->alloc_xftfont (xrm->get (".font.scale","luxi:pixelsize=9"));

    Bst0.font = XftFonts [F_BUTT];
    Bst0.type = X_button_style::RAISED;
    Bst0.color.bg[0] = XftColors [C_BUTT_BG0]->pixel;
    Bst0.color.fg[0] = XftColors [C_BUTT_FG0];
    Bst0.color.bg[1] = XftColors [C_BUTT_BG1]->pixel;
    Bst0.color.fg[1] = XftColors [C_BUTT_FG1];
    Bst0.color.bg[2] = XftColors [C_BUTT_BG2]->pixel;
    Bst0.color.fg[2] = XftColors [C_BUTT_FG2];
    Bst0.color.bg[3] = XftColors [C_BUTT_BG3]->pixel;
    Bst0.color.fg[3] = XftColors [C_BUTT_FG3];
    Bst0.color.shadow.bgnd = XftColors [C_MAIN_BG]->pixel;
    Bst0.color.shadow.lite = XftColors [C_MAIN_LS]->pixel;
    Bst0.color.shadow.dark = XftColors [C_MAIN_DS]->pixel;

    Bst1.font = 0;
    Bst1.type = X_button_style::PLAIN;
    Bst1.color.bg [0] = XftColors [C_MAIN_BG]->pixel;
    Bst1.color.fg [0] = XftColors [C_TICK0];
    Bst1.color.bg [1] = XftColors [C_MAIN_BG]->pixel;
    Bst1.color.fg [1] = XftColors [C_TICK1];

    Bst2.font = 0;
    Bst2.type = X_button_style::BORDER;
    Bst2.color.bg [0] = XftColors [C_MAIN_BG]->pixel;
    Bst2.color.fg [0] = XftColors [C_MAIN_FG];
    Bst2.color.bg [1] = XftColors [C_MAIN_SB]->pixel;
    Bst2.color.fg [1] = XftColors [C_MAIN_FG];
    Bst2.color.shadow.bgnd = XftColors [C_MAIN_BG]->pixel;
    Bst2.color.shadow.lite = XftColors [C_MAIN_LS]->pixel;
    Bst2.color.shadow.dark = XftColors [C_MAIN_DS]->pixel;

    Bst3.font = XftFonts [F_BUTT];
    Bst3.type = X_button_style::PLAIN;
    Bst3.color.bg [0] = XftColors [C_MAIN_BG]->pixel;
    Bst3.color.fg [0] = XftColors [C_MAIN_HL];
    Bst3.color.bg [1] = XftColors [C_MAIN_HL]->pixel;
    Bst3.color.fg [1] = XftColors [C_MAIN_BG];
    Bst3.color.shadow.bgnd = XftColors [C_MAIN_BG]->pixel;
    Bst3.color.shadow.lite = XftColors [C_MAIN_LS]->pixel;
    Bst3.color.shadow.dark = XftColors [C_MAIN_DS]->pixel;

    Tst0.font = XftFonts [F_TEXT];
    Tst0.color.normal.bgnd = XftColors [C_TEXT_BG]->pixel;
    Tst0.color.normal.text = XftColors [C_TEXT_FG];
    Tst0.color.focus.bgnd  = XftColors [C_TEXT_HL]->pixel;
    Tst0.color.focus.text  = XftColors [C_TEXT_FG];
    Tst0.color.focus.line  = XftColors [C_TEXT_CA]->pixel;
    Tst0.color.shadow.lite = XftColors [C_MAIN_LS]->pixel;
    Tst0.color.shadow.dark = XftColors [C_MAIN_DS]->pixel;
    Tst0.color.shadow.bgnd = XftColors [C_MAIN_BG]->pixel;

    Tst1.font = XftFonts [F_TEXT];
    Tst1.color.normal.bgnd = XftColors [C_MAIN_BG]->pixel;
    Tst1.color.normal.text = XftColors [C_MAIN_FG];
    Tst1.color.shadow.lite = XftColors [C_MAIN_LS]->pixel;
    Tst1.color.shadow.dark = XftColors [C_MAIN_DS]->pixel;
    Tst1.color.shadow.bgnd = XftColors [C_MAIN_BG]->pixel;

    Tst2.font = XftFonts [F_TEXT];
    Tst2.color.normal.bgnd = XftColors [C_MAIN_BG]->pixel;
    Tst2.color.normal.text = XftColors [C_MAIN_HL];
    Tst2.color.shadow.lite = XftColors [C_MAIN_LS]->pixel;
    Tst2.color.shadow.dark = XftColors [C_MAIN_DS]->pixel;
    Tst2.color.shadow.bgnd = XftColors [C_MAIN_BG]->pixel;

    Fst1.bg   = XftColors [C_MAIN_BG]->pixel;
    Fst1.lite = XftColors [C_MAIN_LS]->pixel;
    Fst1.dark = XftColors [C_MAIN_DS]->pixel;
    Fst1.knob = XftColors [C_SLID_KN]->pixel;
    Fst1.mark = XftColors [C_SLID_MK]->pixel;
    Fst1.h = 19;
    Fst1.w = 10; 

    Lst0.font = XftFonts [F_FILE];
    Lst0.bg = disp->whitepixel ();
    Lst0.fg [0] = disp->alloc_xftcolor ("blue",    0);
    Lst0.fg [1] = disp->alloc_xftcolor ("#008000", 0);
    Lst0.fg [2] = disp->alloc_xftcolor ("red",     0);
    Lst0.fg [3] = disp->alloc_xftcolor ("red",     0);
    Lst0.dy = Lst0.font->ascent + Lst0.font->descent + 3;

    Sdb1.bg = XftColors [C_DISP_BG]->pixel;
    Sdb1.fg = XftColors [C_MAIN_HL];
    Sdb1.marg = 2;
    Sdb1.font = XftFonts [F_SCAL];
    Sdb1.nseg = 10;
    Sdb1.set_tick ( 0,   1, 1.00e-4f, 0    );
    Sdb1.set_tick ( 1,  12, 1.00e-3f, "-60");
    Sdb1.set_tick ( 2,  24, 3.16e-3f, 0    );
    Sdb1.set_tick ( 3,  36, 1.00e-2f, "-40");
    Sdb1.set_tick ( 4,  56, 3.16e-2f, "-30");
    Sdb1.set_tick ( 5,  81, 1.00e-1f, "-20");
    Sdb1.set_tick ( 6, 101, 1.78e-1f, 0    );
    Sdb1.set_tick ( 7, 121, 3.16e-1f, "-10");
    Sdb1.set_tick ( 8, 137, 0.500f,   0    );
    Sdb1.set_tick ( 9, 149, 0.707f,   0    );
    Sdb1.set_tick (10, 161, 1.000f,   "0"  );

    Sdb2.bg = XftColors [C_MAIN_BG]->pixel;
    Sdb2.fg = XftColors [C_MAIN_FG];
    Sdb2.marg = 0;
    Sdb2.font = XftFonts [F_SCAL];
    Sdb2.nseg = 6;
    Sdb2.set_tick (0,  10, 0.000f,  0    );
    Sdb2.set_tick (1,  26, 0.010f,  "-40");
    Sdb2.set_tick (2,  44, 0.032f,  "-30");
    Sdb2.set_tick (3,  66, 0.100f,  "-20");
    Sdb2.set_tick (4,  93, 0.316f,  "-10");
    Sdb2.set_tick (5, 121, 1.000f,  "0"  );
    Sdb2.set_tick (6, 151, 3.162f,  "10" );
}


void styles_fini (X_display *disp)
{
}
