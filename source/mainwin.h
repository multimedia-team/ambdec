// ----------------------------------------------------------------------------
//
//  Copyright (C) 2006-2011 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 2 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program; if not, write to the Free Software
//  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// ----------------------------------------------------------------------------


#ifndef __MAINWIN_H
#define	__MAINWIN_H


#include <clxclient.h>
#include "global.h"
#include "jclient.h"
#include "adconf.h"
#include "confwin.h"
#include "meter.h"


class Mainwin : public A_thread, public X_window, public X_callback
{
public:

    Mainwin (X_rootwin *parent, X_resman *xres, int xp, int yp, Jclient *jclient);
    ~Mainwin (void);
    Mainwin (const Mainwin&);
    Mainwin& operator=(const Mainwin&);

    void stop (void) { _stop = true; }
    void loadconf (void);
    int  process (void);

private:

    enum { B_CONF = MAXOP, B_INTT, B_EXTT, B_UP, B_DN };
  
    virtual void thr_main (void) {}

    void handle_time (void);
    void handle_stop (void);
    void handle_event (XEvent *);
    void handle_callb (int type, X_window *W, XEvent *E);
    void expose (XExposeEvent *E);
    void clmesg (XClientMessageEvent *E);
    void syncdec (int e);
    void applconf (void);
    void editconf (void);
    void makedisp (void);
    void reset (void);
    void confdisp (void);
    void drawdisp (void);
    void testbutt (int b);
    void chanbutt (int b, XButtonEvent *E);

    Atom            _atom;
    bool            _stop;
    X_resman       *_xres;
    Jclient        *_jclient;
    Confwin        *_confwin;
    X_slider       *_fvol;
    X_subwin       *_disp;
    X_vscale       *_sclt;
    X_vscale       *_scrt;
    Meter          *_dlev [MAXOP];
    X_tbutton      *_btst [MAXOP];
    X_tbutton      *_bconf;
    X_tbutton      *_bintt;
    X_tbutton      *_bextt;
    X_ibutton      *_bup;
    X_ibutton      *_bdn;
    bool            _fixop;
    int             _nout;
    int             _nact;
    int             _xm, _ym;
    int             _xd, _yd;
    AD_conf         _conf;
};


#endif
