# ----------------------------------------------------------------------------
#
#  Copyright (C) 2006-2018 Fons Adriaensen <fons@linuxaudio.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ----------------------------------------------------------------------------


PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
SHARED ?= $(PREFIX)/share/ambdec

VERSION = 0.7.1
CPPFLAGS += -MMD -MP -DVERSION=\"$(VERSION)\" -DSHARED=\"$(SHARED)\"
CXXFLAGS += -O2 -ffast-math -Wall
CXXFLAGS += -march=native


all:	ambdec ambdec_cli


AMBDEC_O = ambdec.o styles.o mainwin.o confwin.o radbut.o filewin.o \
           jclient.o nffilt.o xover2.o decoder.o adconf.o sstring.o \
           png2img.o meter.o
ambdec:	CPPFLAGS += $(shell pkgconf --cflags freetype2)
ambdec:	LDLIBS += -lclxclient -lclthreads -ljack -lpng -lpthread -lXft -lX11 -lrt
ambdec:	$(AMBDEC_O)
	$(CXX) $(LDFLAGS) -o $@ $(AMBDEC_O) $(LDLIBS)
$(AMBDEC_O):
-include $(AMBDEC_O:%.o=%.d)



AMBDEC_CLI_O = ambdec_cli.o jclient.o nffilt.o xover2.o decoder.o adconf.o sstring.o
ambdec_cli:	LDLIBS += -lclthreads -lpthread -ljack -lrt
ambdec_cli:	$(AMBDEC_CLI_O)
	$(CXX) $(LDFLAGS) -o $@ $(AMBDEC_CLI_O) $(LDLIBS)
$(AMBDEC_CLI_O):
-include $(AMBDEC_CLI_O:%.o=%.d)


install:	all
	install -d $(DESTDIR)$(BINDIR)
	install -d $(DESTDIR)$(SHARED)
	install -m 755 ambdec     $(DESTDIR)$(BINDIR)
	install -m 755 ambdec_cli $(DESTDIR)$(BINDIR)
	install -m 644 ../share/* $(DESTDIR)$(SHARED)


uninstall:
	rm -f $(DESTDIR)$(BINDIR)/ambdec
	rm -f $(DESTDIR)$(BINDIR)/ambdec_cli
	rm -rf $(DESTDIR)$(SHARED)


clean:
	rm -f *~ *.o *.a *.d *.so
	rm -f ambdec
	rm -f ambdec_cli

